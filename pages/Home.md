Welcome to the speedie.site wiki!
---------------------------------

Welcome to the speedie.site wiki! This is a wiki that anyone may edit. It is mostly related to speedie.site projects such as fontctrl, spmenu and others, although [speedwm](https://speedwm.speedie.site) has its own wiki.