# [spmenu] How to use the buildconf

This wiki article describes how to use the buildconf. It's really simple, and there's not much to say.

If you cloned spmenu from the [git repository](https://git.speedie.gq/spmenu), you should already have a default buildconf. This same config is used to build the Arch/Gentoo packages.

# Toggle image support

If you want to enable/disable image support (and as such, the `imlib2` dependency), you may set `imlib2=true` or `imlib2=false` in the buildconf.