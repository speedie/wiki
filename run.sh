#!/bin/sh
#
# crontab this to run every X minutes
# 5 * * * * /usr/bin/run.sh

die() { printf "%s\n" "$*"; exit 1; }

WIKINAME="wiki"
POST_DIR="$1"
[ -z "$1" ] && POST_DIR="/var/www/page/$WIKINAME/pages"

[ -e "$POST_DIR" ] || die "You must specify a VALID path. Edit $0."

cd "$POST_DIR" || die "Does not exist."

pwd

[ -e "/tmp/markdown-log" ] && mv /tmp/markdown-log /tmp/markdown-log-2
find ./*md > /tmp/markdown-log

[ -e "/tmp/markdown-log-2" ] || exit 0

[ -z "$(diff -up /tmp/markdown-log /tmp/markdown-log-2)" ] && exit 0

git add ./*.md
git commit -am "Add articles"

printf "%s - Added articles" "$(date)" > /tmp/markdown-crontab-log

exit 0
